#!/bin/bash
envsubst < /tmp/mssql/initialize-template.sql > /tmp/mssql/initialize.sql

for i in {1..50};
do
    /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P "$SA_PASSWORD" -d master -i /tmp/mssql/initialize.sql
    if [ $? -eq 0 ]
    then
        echo "setup.sql completed"
        break
    else
        sleep 1
    fi
done
