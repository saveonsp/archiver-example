from mcr.microsoft.com/mssql/server

user root
run apt-get update && \
  apt-get install -y gettext-base && \
  rm -rf /var/lib/apt/lists/* && \
  mkdir /tmp/mssql && \
  chown -R mssql /tmp/mssql && \
  mkdir /data && \
  chown -R mssql /data

user mssql
volume /data
env ACCEPT_EULA=Y
env SA_PASSWORD=Password--
env MSSQL_TCP_PORT=1433


copy db/ /tmp/mssql/
cmd ["/tmp/mssql/entrypoint.sh"]
