const fs = require('fs');
const fsp = require('fs/promises');
const path = require('path');
const http = require('http');
const { strict: assert } = require('assert');
const { Duplex } = require('stream');
const { pipeline, finished } = require('stream/promises');
const { setTimeout } = require('timers/promises');

require('dotenv').config()
const sql = require('mssql')
const express = require('express');
const archiver = require('archiver');
const puppeteer = require('puppeteer');
const stringify = require('csv-stringify');

class Processor extends Duplex {
  queue = [];
  running = 0;

  // accepts processor factory that initializes async generator
  constructor({concurrency, createProcessor, ...options} = {concurrency: 1}) {
    super({highWaterMark: 10, ...options, objectMode: true});
    this.createProcessor = createProcessor;
    this.concurrency = concurrency;
  }

  async cleanup() {
    await Promise.all(this.processors.map(processor => processor.next(null)));
  }

  // check the queue for available input. pass to processor, if available
  check() {
    while (this.queue.length > 0 && this.processors.length > 0) {
      const [chunk, callback] = this.queue.shift();
      this.process(chunk);
      callback();
    }
    if (this.finalize && this.running === 0) {
      this.push(null);
      this.finalize();
    }
  }

  async process(chunk) {
    this.running++;
    const processor = this.processors.shift();
    try {
      const { done, value } = await processor.next(chunk);
      // should assert done != true

      // should check return value
      this.push(value);

    } catch (err) {
      this.destroy(err);
    } finally {
      this.running--;
      this.processors.push(processor);
      this.check();
    }
  }

  // initialize processors based on concurrency
  async _construct(callback) {
    try {
      this.processors = await Promise.all(Array.from(Array(this.concurrency), () => this.createProcessor()));
      callback();
    } catch (err) {
      callback(err);
    }
  }

  // send null to processors, allow for cleanup
  async _destroy(originalError, callback) {
    try {
      await this.cleanup();
      callback(originalError);
    } catch (err) {
      callback(originalError || err);
    }
  }


  // accept record, start processing on each
  _write(chunk, encoding, callback) {
    if (this.processors.length > 0) {
      this.process(chunk);
      callback();
    } else {
      this.queue.push([chunk, callback]);
    }
  }

  // should use this?
  _read(size) {
  }

  // called when source stream is complete.  wait for running processes to complete
  _final(callback) {
    if (this.queue.length > 0 || this.running > 0) {
      this.finalize = callback;
    } else {
      callback();
    }
  }
}


(async () => {
  const port = 3000;
  const origin = `http://localhost:${port}`;
  const app = express();
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  const sqlConfig = {
    user: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME,
    server: 'localhost',
    options: {
      encrypt: false,
    },
  };
  await sql.connect(sqlConfig);

  for (const p of await fsp.readdir('.', {withFileTypes: true})) {
    if (p.isFile() && path.extname(p.name) === '.zip') {
      await fsp.unlink(p.name);
    }
  }
  await sql.query`
    drop table if exists records
  `;
  await sql.query`
    create table records(id int not null identity primary key, title varchar(100) not null);
  `;

  // bulk insert
  {
    const table = new sql.Table('records');
    table.columns.add('title', sql.VarChar(100), {nullable: false});
    for (let i = 0; i < 2000; i++)
      table.rows.add(`Record ${i}`);
    const request = new sql.Request();
    await request.bulk(table);
  }

  app.get('/', (req, res) => {
    res.send('toast');
  });

  app.get('/stream', (req, res) => {
    const request = new sql.Request();
    request.stream = true;
    request.query(`select id, title from records`);

    request.on('row', row => {
      res.write(`${JSON.stringify(row)}\n`);
    });

    request.on('done', () => res.end());
  });

  const createProcessor = async () => {
    const g = (async function * (_browser) {
      let input, value;
      const page = await _browser.newPage();
      console.log('processor created');
      try {
        while (true) {
          const input = yield value;
          if (input == null) {
            break;
          }

          const html = `
          <html>
            <head></head>
            <body>
              <pre>${JSON.stringify(input)}</pre>
            </body>
          </html>
          `;
          await page.setContent(html, { waitUntil: 'domcontentloaded' });
          const buf = await page.pdf();
          value = {buf, filename: input.id + '.pdf'};
        }
      } finally {
        await page.close();
        console.log('processor destroyed');
      }
    })(browser);
    await g.next();
    return g;
  }


  app.get('/page', (req, res) => {
    res.json(req.query);
  });

  app.get('/pdf', async (req, res) => {
    const archive = archiver('zip', { zlib: { level: 9 }});

    archive.pipe(res);

    await page.goto(`${origin}/`, { waitUntil: 'domcontentloaded' });
    const buf = await page.pdf();
    archive.append(buf, { name: 'page.pdf' });
    archive.finalize();

    // await new Promise(resolve => {
    //   output.on('close', function() {
    //     console.log(archive.pointer() + ' total bytes');
    //     resolve();
    //   });
    //   archive.finalize();
    // });
  });

  app.get('/dump', async (req, res) => {
    const request = new sql.Request();
    const requestStream = request.toReadableStream({highWaterMark: 10});
    // request.on('done', () => res.end());

    // const ac = new AbortController();
    // const options = {
    //   signal: ac.signal,
    // };

    request.query(`select id, title from records`);
  
    const filename = `DrugLists.zip`;
    const outputPath = path.join(__dirname, filename);

    let cacheExists = false;
    try {
      const stats = await fsp.stat(outputPath);
      assert.ok(stats.isFile());
      cacheExists = true;
    } catch (err) {}

    res.append('Content-disposition', `attachment; filename=${filename}`);

    if (cacheExists) {
      fs.createReadStream(outputPath).pipe(res);
    } else {
      const output = fs.createWriteStream(outputPath);
      const archive = archiver('zip', { zlib: { level: 9 }});

      const duplex = new Processor({createProcessor, concurrency: 4});


      archive.pipe(output);
      archive.pipe(res);

      const s = requestStream.pipe(duplex);

      s.on('data', ({buf, filename}) => {
        archive.append(buf, { name: filename });
      });

      // for await (const {buf, filename} of requestStream.pipe(duplex)) {
      //   archive.append(buf, { name: filename });
      // }
      s.on('end', () => {
        archive.finalize();
      });

      // await finished(output);
    }
  });


  const server = http.createServer(app);
  await new Promise(resolve => server.listen(port).once('listening', () => resolve()));
  
  {
    // output
    const outputPath = path.join(__dirname, 'example1.zip');
    const output = fs.createWriteStream(outputPath);
    const archive = archiver('zip', { zlib: { level: 9 }});

    output.on('end', function() {
      console.log('Data has been drained');
    });
    archive.on('error', function(err) {
      throw err;
    });
    archive.pipe(output);
    
    await page.goto(`${origin}/`, { waitUntil: 'domcontentloaded' });
    const buf = await page.pdf();
    archive.append(buf, { name: 'page.pdf' });

    await new Promise(resolve => {
      output.on('close', function() {
        console.log(archive.pointer() + ' total bytes');
        resolve();
      });
      archive.finalize();
    });
  }



  {
    const outputPath = path.join(__dirname, 'example2.zip');
    const output = fs.createWriteStream(outputPath);

    await new Promise(resolve => http.get(`${origin}/pdf`, res => {
      res.pipe(output);

      res.on('end', () => resolve());
    }));
  }

  {
    const outputPath = path.join(__dirname, 'example3.txt');
    const output = fs.createWriteStream(outputPath);

    await new Promise(resolve => http.get(`${origin}/stream`, res => {
      res.pipe(output);

      res.on('end', () => resolve());
    }));
  }

  {

    // await pipeline(
    //   // fetch data
    //   // transform data to pdf (filename/buffer)
    //   // transform via archiver
    //   // write to response object
    //   request,
    //   new Transform({
    //     objectMode: true,
    //     highWaterMark: 10,
    //     transform(chunk, encoding, callback) {
    //       console.log('chunk', chunk);
    //       setTimeout(() => callback(null, chunk), 1000);
    //     },
    //   }),
    //   options,
    // )
  }

  await setTimeout(100000);
  await browser.close();
  server.close();
  await sql.close();
})();


async function doWork(page, input) {
  // do something with a page and input
  await setTimeout(1000);
  return input;
}


function createProcessor(browser) {
  return async function* (source) {
    const page = await browser.newPage();
    for await (const input of source) {
      const result = await doWork(page, input);
      yield result;
    }
  }
}

